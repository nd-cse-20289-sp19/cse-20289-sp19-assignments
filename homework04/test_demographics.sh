#!/bin/bash

# Configuration

SCRIPT=demographics.py
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

# Functions

error() {
    echo "$@"
    [ -r $WORKSPACE/log ] && cat $WORKSPACE/log
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

default_output() {
    cat <<EOF
	2013	2014	2015	2016	2017	2018	2019	2020	2021
================================================================================
   M	  49	  44	  58	  60	  65	 101	  96	  93	  91	
   F	  14	  12	  16	  19	  26	  45	  54	  36	  47	
--------------------------------------------------------------------------------
   B	   3	   2	   4	   1	   5	   3	   3	   4	   6	
   C	  43	  43	  47	  53	  60	 107	  96	  87	  94	
   N	   1	   1	   1	   7	   5	   5	  13	  12	  14	
   O	   7	   5	   9	   9	  12	  10	  13	  10	   8	
   S	   7	   4	  10	   9	   3	  13	  10	   9	  10	
   T	   2	   1	   1	   0	   6	   8	  15	   6	   5	
   U	   0	   0	   2	   0	   0	   0	   0	   1	   1	
--------------------------------------------------------------------------------
EOF
}

default_y2013_output() {
    cat <<EOF
	2013
================
   M	  49	
   F	  14	
----------------
   B	   3	
   C	  43	
   N	   1	
   O	   7	
   S	   7	
   T	   2	
   U	   0	
----------------
EOF
}

default_y2017_2019_2021_output() {
    cat <<EOF
	2017	2019	2021
================================
   M	  65	  96	  91	
   F	  26	  54	  47	
--------------------------------
   B	   5	   3	   6	
   C	  60	  96	  94	
   N	   5	  13	  14	
   O	  12	  13	   8	
   S	   3	  10	  10	
   T	   6	  15	   5	
   U	   0	   0	   1	
--------------------------------
EOF
}

default_p_output() {
    cat <<EOF
	2013	2014	2015	2016	2017	2018	2019	2020	2021
================================================================================
   M	77.8%	78.6%	78.4%	75.9%	71.4%	69.2%	64.0%	72.1%	65.9%	
   F	22.2%	21.4%	21.6%	24.1%	28.6%	30.8%	36.0%	27.9%	34.1%	
--------------------------------------------------------------------------------
   B	 4.8%	 3.6%	 5.4%	 1.3%	 5.5%	 2.1%	 2.0%	 3.1%	 4.3%	
   C	68.3%	76.8%	63.5%	67.1%	65.9%	73.3%	64.0%	67.4%	68.1%	
   N	 1.6%	 1.8%	 1.4%	 8.9%	 5.5%	 3.4%	 8.7%	 9.3%	10.1%	
   O	11.1%	 8.9%	12.2%	11.4%	13.2%	 6.8%	 8.7%	 7.8%	 5.8%	
   S	11.1%	 7.1%	13.5%	11.4%	 3.3%	 8.9%	 6.7%	 7.0%	 7.2%	
   T	 3.2%	 1.8%	 1.4%	 0.0%	 6.6%	 5.5%	10.0%	 4.7%	 3.6%	
   U	 0.0%	 0.0%	 2.7%	 0.0%	 0.0%	 0.0%	 0.0%	 0.8%	 0.7%	
--------------------------------------------------------------------------------
EOF
}

default_G_output() {
    cat <<EOF
	2013	2014	2015	2016	2017	2018	2019	2020	2021
================================================================================
   B	   3	   2	   4	   1	   5	   3	   3	   4	   6	
   C	  43	  43	  47	  53	  60	 107	  96	  87	  94	
   N	   1	   1	   1	   7	   5	   5	  13	  12	  14	
   O	   7	   5	   9	   9	  12	  10	  13	  10	   8	
   S	   7	   4	  10	   9	   3	  13	  10	   9	  10	
   T	   2	   1	   1	   0	   6	   8	  15	   6	   5	
   U	   0	   0	   2	   0	   0	   0	   0	   1	   1	
--------------------------------------------------------------------------------
EOF
}

default_E_output() {
    cat <<EOF
	2013	2014	2015	2016	2017	2018	2019	2020	2021
================================================================================
   M	  49	  44	  58	  60	  65	 101	  96	  93	  91	
   F	  14	  12	  16	  19	  26	  45	  54	  36	  47	
--------------------------------------------------------------------------------
EOF
}

equality_output() {
    cat <<EOF
	2013	2014	2015	2016	2017	2018	2019
================================================================
   M	   1	   1	   1	   1	   1	   1	   1	
   F	   1	   1	   1	   1	   1	   1	   1	
----------------------------------------------------------------
   B	   2	   0	   0	   0	   0	   0	   0	
   C	   0	   2	   0	   0	   0	   0	   0	
   N	   0	   0	   2	   0	   0	   0	   0	
   O	   0	   0	   0	   2	   0	   0	   0	
   S	   0	   0	   0	   0	   2	   0	   0	
   T	   0	   0	   0	   0	   0	   2	   0	
   U	   0	   0	   0	   0	   0	   0	   2	
----------------------------------------------------------------
EOF
}

equality_y2016_p_output() {
    cat <<EOF
	2016
================
   M	50.0%	
   F	50.0%	
----------------
   B	 0.0%	
   C	 0.0%	
   N	 0.0%	
   O	100.0%	
   S	 0.0%	
   T	 0.0%	
   U	 0.0%	
----------------
EOF
}

equality_y2016_p_E_output() {
    cat <<EOF
	2016
================
   M	50.0%	
   F	50.0%	
----------------
EOF
}

equality_y2016_p_E_G_output() {
    cat <<EOF
	2016
================
EOF
}

# Setup

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

# Tests

echo "Testing $SCRIPT ..."

printf "   %-40s ... " "Bad arguments"
./$SCRIPT -bad &> /dev/null
if [ $? -eq 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "-h"
./$SCRIPT -h 2>&1 | grep -i usage &> /dev/null
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "No arguments"
diff -y <(./$SCRIPT) <(default_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "MtP"
diff -y <(./$SCRIPT https://yld.me/raw/MtP) <(default_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "MtP -y 2013"
diff -y <(./$SCRIPT -y 2013) <(default_y2013_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "MtP -y 2017,2021,2019"
diff -y <(./$SCRIPT -y 2017,2021,2019) <(default_y2017_2019_2021_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "MtP -p"
diff -y <(./$SCRIPT -p) <(default_p_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "MtP -G"
diff -y <(./$SCRIPT -G) <(default_G_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "MtP -E"
diff -y <(./$SCRIPT -E) <(default_E_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "ilG"
diff -y <(./$SCRIPT https://yld.me/raw/ilG) <(equality_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "ilG -y 2016 -p"
diff -y <(./$SCRIPT -y 2016 -p https://yld.me/raw/ilG) <(equality_y2016_p_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "ilG -y 2016 -p -E"
diff -y <(./$SCRIPT -y 2016 -p -E https://yld.me/raw/ilG) <(equality_y2016_p_E_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

printf "   %-40s ... " "ilG -y 2016 -p -E -G"
diff -y <(./$SCRIPT -y 2016 -p -E -G https://yld.me/raw/ilG) <(equality_y2016_p_E_G_output) &> $WORKSPACE/log
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo  "Success"
fi

TESTS=$(($(grep -c Success $0) - 1))

echo
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 4.0" | bc | awk '{printf "%0.2f\n", $1}')"
echo
