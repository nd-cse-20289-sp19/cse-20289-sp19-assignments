#!/usr/bin/env python3

import collections
import os
import sys

import requests

# Globals

URL     = 'https://yld.me/raw/MtP'
GENDERS = ('M', 'F')
ETHNICS = ('B', 'C', 'N', 'O', 'S', 'T', 'U')

# Functions

def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] URL

    -y  YEARS   Which years to display (default: all)
    -p          Display data as percentages.
    -G          Do not include gender information.
    -E          Do not include ethnic information.
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def load_demo_data(url=URL):
    ''' Load demographics from specified URL into dictionary '''
    return None

def dump_demo_data(data, years=None, percent=False, gender=True, ethnic=True):
    ''' Dump demographics data for the specified years and attributes '''
    pass

def dump_demo_separator(years, char='='):
    ''' Dump demographics separator '''
    pass

def dump_demo_years(years):
    ''' Dump demographics years information '''
    pass

def dump_demo_fields(data, years, fields, percent=False):
    ''' Dump demographics information (for particular fields) '''
    pass

def dump_demo_gender(data, years, percent=False):
    ''' Dump demographics gender information '''
    pass

def dump_demo_ethnic(data, years, percent=False):
    ''' Dump demographics ethnic information '''
    pass

# Parse Command-line Options

args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)

# Main Execution
