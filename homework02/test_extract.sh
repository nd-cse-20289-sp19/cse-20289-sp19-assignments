#!/bin/bash

# Configuration

SCRIPT=extract.sh
WORKSPACE=${TMPDIR:-/tmp}/${SCRIPT}.$(id -u)
NSOURCE=5
FAILURES=0

# Utilities

error() {
    echo "$@"
    [ -r $WORKSPACE/test.log ] && (echo ; cat $WORKSPACE/test.log; echo)
    FAILURES=$(($FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

test_script() {
    rm -fr $WORKSPACE/data
    if ! (cd $WORKSPACE; ./$SCRIPT $@ &> $WORKSPACE/test.log && test -d data); then
    	error "Failure"
    else
    	echo  "Success"
    fi
}

# Main execution

## 1. Create workspace

mkdir $WORKSPACE
trap "cleanup 1" INT TERM HUP QUIT STOP
trap "cleanup" EXIT

## 2. Copy script to workspace

cp $SCRIPT $WORKSPACE

## 3. Create archives

mkdir -p $WORKSPACE/data

for i in $(seq 1 $NSOURCE); do
    touch $WORKSPACE/data/file.$i
done
(cd $WORKSPACE ; tar czf $WORKSPACE/archive.tgz data ; ln archive.tgz archive.tar.gz)
(cd $WORKSPACE ; tar cjf $WORKSPACE/archive.tbz data ; ln archive.tbz archive.tar.bz2)
(cd $WORKSPACE ; tar cJf $WORKSPACE/archive.txz data ; ln archive.txz archive.tar.xz)
(cd $WORKSPACE ; zip -qr $WORKSPACE/archive.zip data ; ln archive.zip archive.jar)

cp /etc/hosts $WORKSPACE/failure.tar.gz

## 4. Test script

echo "Testing $SCRIPT ..."

printf "%-40s ... " "Usage"
if ! $WORKSPACE/$SCRIPT | grep -i 'usage' > /dev/null; then
    error "Failure"
else
    echo  "Success"
fi

printf "%-40s ... " "Loop"
if ! grep -qE '^(for.*\$@|[[:space:]]*while)' $WORKSPACE/$SCRIPT; then
    error "Failure"
else
    echo  "Success"
fi

for ext in tgz tar.gz tbz tar.bz2 txz tar.xz zip jar; do
    printf "%-40s ... " "$ext"
    test_script archive.$ext
done

printf "%-40s ... " "Failure"
if $WORKSPACE/$SCRIPT $WORKSPACE/failure.tar.gz &> $WORKSPACE/test.log; then
    error "Failure"
else
    echo  "Success"
fi

printf "%-40s ... " "Multiple"
test_script archive.tgz archive.tar.bz2 archive.txz archive.zip

printf "%-40s ... " "Unknown"
if ! ${WORKSPACE}/${SCRIPT} taco.cat | grep -i 'unknown' > /dev/null; then
    error "Failure"
else
    echo  "Success"
fi

## 6. Report test results

TESTS=$(($(grep -c Success $0) + 8 - 1))
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 2.00" | bc | awk '{printf "%0.2f\n", $0}')"
echo
