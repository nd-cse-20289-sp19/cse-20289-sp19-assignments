#!/bin/bash

# Configuration

SCRIPT=crack.sh
WORKSPACE=${TMPDIR:-/tmp}/${SCRIPT}.$(id -u)
FAILURES=0

# Utilities

error() {
    echo "$@"
    [ -r $WORKSPACE/test.log ] && (echo ; cat $WORKSPACE/test.log; echo)
    FAILURES=$(($FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

test_script() {
    rm -fr $WORKSPACE/data
    if ! (cd $WORKSPACE; ./$SCRIPT $@ &> $WORKSPACE/test.log && test -d data); then
    	error "Failure"
    else
    	echo  "Success"
    fi
}

test_output() {
    cat <<EOF
Password is goirish
Token    is domer2019
EOF
}

# Main execution

## 1. Create workspace

mkdir $WORKSPACE
trap "cleanup 1" INT TERM HUP QUIT STOP
trap "cleanup" EXIT

## 2. Copy script to workspace

cp $SCRIPT $WORKSPACE

## 3. Test script

echo "Testing $SCRIPT ..."

printf "%-40s ... " "Usage"
if ! $WORKSPACE/$SCRIPT | grep -i 'usage' > /dev/null; then
    error "Failure"
else
    echo  "Success"
fi

printf "%-40s ... " "Readable"
if ! $WORKSPACE/$SCRIPT $WORKSPACE/crackable.sh | grep -q -i readable; then
    error "Failure"
else
    echo  "Success"
fi

cat > $WORKSPACE/crackable.sh <<EOF
#!/bin/sh

if [ "\$1" = "goirish" ]; then
    echo "domer2019"
    exit 0
fi

PASSWORDS="
domers
notredame
goirish
"
exit 1
EOF

printf "%-40s ... " "Executable"
if ! $WORKSPACE/$SCRIPT $WORKSPACE/crackable.sh | grep -q -i executable; then
    error "Failure"
else
    echo  "Success"
fi

chmod +x $WORKSPACE/crackable.sh

printf "%-40s ... " "Crackable"
if ! diff -y <($WORKSPACE/$SCRIPT $WORKSPACE/crackable.sh) <(test_output) &> $WORKSPACE/test.log; then
    error "Failure"
    FAILURES=$(($FAILURES + 1))
    FAILURES=$(($FAILURES + 1))
else
    echo  "Success"
    # Success
    # Success
fi

cat > $WORKSPACE/crackable.sh <<EOF
exit 1
EOF

printf "%-40s ... " "Uncrackable"
if ! $WORKSPACE/$SCRIPT $WORKSPACE/crackable.sh | grep -q -i unable; then
    error "Failure"
else
    echo  "Success"
fi

## 4. Report test results

TESTS=$(($(grep -c Success $0) - 1))
echo "   Score $(echo "scale=4; ($TESTS - $FAILURES) / $TESTS.0 * 2.00" | bc | awk '{printf "%0.2f\n", $0}')"
echo
