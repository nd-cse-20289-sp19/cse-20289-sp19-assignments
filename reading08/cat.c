/* cat.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    ____(stderr, "Usage: %s FILES...\n", PROGRAM_NAME);                         /* 1 */
    ____(status);                                                               /* 2 */
}

void cat_stream(FILE *stream) {
    char buffer[BUFSIZ];

    while (____(buffer, BUFSIZ, stream)) {                                      /* 3 */
        ____(buffer, stdout);                                                   /* 4 */
    }
}

void cat_file(const char *path) {
    FILE *fs = ____(path, "r");                                                 /* 5 */
    if (fs == NULL) {
        fprintf(stderr, "%s: %s: %s\n", PROGRAM_NAME, path, ____(errno));       /* 6 */
        return;
    }
    cat_stream(fs);
    ____(fs);                                                                   /* 7 */
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int argind = 1;

    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];
    while (argind < argc && ____(argv[argind]) > 1 && argv[argind][0] == '-') { /* 8 */
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            default:
                usage(1);
                break;
        }
    }

    /* Process each file */
    if (argind == argc) {
        cat_stream(stdin);
    } else {
        while (argind < argc) {
            char *path = argv[argind++];
            if (____(path, "-") == 0) {                                         /* 9 */
                cat_stream(stdin);
            } else {
                cat_file(path);
            }
        }
    }

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
