#!/usr/bin/env python3

import ctypes
import os
import stat
import sys
import tempfile
import unittest

# Constants

SOURCES = ['src/filter.c', 'src/utilities.c']
CFLAGS  = '-std=gnu99 -Wall -Werror -Iinclude -fPIC'
LIBRARY = './libfilters.so'

# Structures

class Options(ctypes.Structure):
    _fields_ = [
        ('access', ctypes.c_int32),
        ('type'  , ctypes.c_int32),
        ('empty' , ctypes.c_bool),
        ('name'  , ctypes.c_char_p),
        ('path'  , ctypes.c_char_p),
        ('perm'  , ctypes.c_int32),
        ('newer' , ctypes.c_uint64),
        ('uid'   , ctypes.c_int32),
        ('gid'   , ctypes.c_int32),
    ]

class TimeSpec(ctypes.Structure):
    _fields_ = [
        ('tv_sec' , ctypes.c_int64),
        ('tv_usec', ctypes.c_int64),
    ]

class Stats(ctypes.Structure):
    _fields_ = [
        ('st_dev'    , ctypes.c_int64),
        ('st_ino'    , ctypes.c_int64),
        ('st_nlink'  , ctypes.c_int64),
        ('st_mode'   , ctypes.c_int32),
        ('st_uid'    , ctypes.c_int32),
        ('st_gid'    , ctypes.c_int32),
        ('st_rdev'   , ctypes.c_int64),
        ('st_size'   , ctypes.c_int64),
        ('st_blksize', ctypes.c_int64),
        ('st_blocks' , ctypes.c_int64),
        ('st_atim'   , TimeSpec),
        ('st_mtim'   , TimeSpec),
        ('st_ctim'   , TimeSpec),
    ]

# Functions

def compile_shared_library(library=LIBRARY, sources=SOURCES, cflags=CFLAGS):
    command = 'gcc {} -shared -o {} {}'.format(cflags, library, ' '.join(sources))
    return os.system(command) == 0

# Unit Tests

class FiltersTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.assertTrue(cls, compile_shared_library())

        cls.Library = ctypes.CDLL(LIBRARY)
        cls.Library.filter_access.restype = ctypes.c_bool
        cls.Library.filter_type.restype   = ctypes.c_bool
        cls.Library.filter_empty.restype  = ctypes.c_bool
        cls.Library.filter_name.restype   = ctypes.c_bool
        cls.Library.filter_path.restype   = ctypes.c_bool
        cls.Library.filter_perm.restype   = ctypes.c_bool
        cls.Library.filter_newer.restype  = ctypes.c_bool
        cls.Library.filter_uid.restype    = ctypes.c_bool
        cls.Library.filter_gid.restype    = ctypes.c_bool

        cls.EmptyDirectory = tempfile.TemporaryDirectory()
        cls.EmptyFile      = tempfile.NamedTemporaryFile()

        cls.Points = 0

    @classmethod
    def tearDownClass(cls):
        os.unlink(LIBRARY)
        print('   Score {:.2f}'.format(cls.Points))

    def test_00_filter_access(self):
        tests = [
            (b'/etc/passwd', os.R_OK, False),
            (b'/bin/ls'    , os.R_OK, False),
            (b'/bin/ls'    , os.X_OK, False),
            (b'/bin/ls'    , os.R_OK|os.X_OK, False),
            (b'/etc/hosts' , os.X_OK, True),
            (b'/bin/asdf'  , os.R_OK, True),
        ]
        for path, access, result in tests:
            options = Options(access)
            stats   = Stats(0)
            self.assertEqual(result, self.Library.filter_access(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5

    def test_01_filter_type(self):
        tests = [
            (b'/bin/ls'    , stat.S_IFREG, False),
            (b'/etc/passwd', stat.S_IFREG, False),
            (b'/bin'       , stat.S_IFDIR, False),
            (b'/etc'       , stat.S_IFDIR, False),
            (b'/bin/ls'    , stat.S_IFDIR, True),
            (b'/etc/passwd', stat.S_IFDIR, True),
            (b'/bin'       , stat.S_IFREG, True),
            (b'/etc'       , stat.S_IFREG, True),
        ]
        for path, ftype, result in tests:
            options = Options(type=ftype)
            stats   = Stats(st_mode = os.stat(path).st_mode)
            self.assertEqual(result, self.Library.filter_type(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5
    
    def test_02_filter_empty(self):
        tests = [
            (self.EmptyDirectory.name.encode(), False),
            (self.EmptyFile.name.encode()     , False),
            (b'/etc'      , True),
            (b'/etc/hosts', True),
        ]
        for path, result in tests:
            options = Options(empty=True)
            stats   = Stats(
                st_mode = os.stat(path).st_mode,
                st_size = os.stat(path).st_size,
            )
            self.assertEqual(result, self.Library.filter_empty(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 1.0
    
    def test_03_filter_name(self):
        tests = [
            (b'/etc/resolv.conf', b'*.conf', False),
            (b'/etc/resolv.conf', b'r*'    , False),
            (b'/etc/hosts'      , b'*.conf', True),
        ]
        for path, pattern, result in tests:
            options = Options(name=pattern)
            stats   = Stats(0)
            self.assertEqual(result, self.Library.filter_name(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5
    
    def test_04_filter_path(self):
        tests = [
            (b'/etc/security/limits.conf', False),
            (b'/etc/resolv.conf'         , True),
        ]
        for path, result in tests:
            options = Options(path=b'*security/*.conf')
            stats   = Stats(st_mode = os.stat(path).st_mode)
            self.assertEqual(result, self.Library.filter_path(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5

    def test_05_filter_perm(self):
        tests = [
            (b'/etc/hosts', 0o644, False),
            (b'/bin/ls'   , 0o755, False),
            (b'/etc/hosts', 0o755, True),
            (b'/bin/ls'   , 0o644, True),
        ]
        for path, perm, result in tests:
            options = Options(perm=perm)
            stats   = Stats(st_mode = os.stat(path).st_mode)
            self.assertEqual(result, self.Library.filter_perm(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5

    def test_06_filter_newer(self):
        tests = [
            (b'/etc/hosts'               , True),
            (self.EmptyFile.name.encode(), False),
        ]
        for path, result in tests:
            options = Options(newer=int(os.stat('/etc/hosts').st_mtime))
            stats   = Stats(0)
            stats.st_mtim.tv_sec = int(os.stat(path).st_mtime)
            self.assertEqual(result, self.Library.filter_newer(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5
    
    def test_07_filter_uid(self):
        tests = [
            (b'/etc/hosts'               , 0, False),
            (self.EmptyFile.name.encode(), 1, True),
        ]
        for path, uid, result in tests:
            options = Options(uid=uid)
            stats   = Stats(st_uid = os.stat(path).st_uid)
            self.assertEqual(result, self.Library.filter_uid(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5
    
    def test_08_filter_gid(self):
        tests = [
            (b'/etc/hosts'               , 0, False),
            (self.EmptyFile.name.encode(), 1, True),
        ]
        for path, gid, result in tests:
            options = Options(gid=gid)
            stats   = Stats(st_gid = os.stat(path).st_gid)
            self.assertEqual(result, self.Library.filter_gid(
                ctypes.c_char_p(path),
                ctypes.byref(stats), 
                ctypes.byref(options),
            ))
            FiltersTestCase.Points += 1 / len(tests) * 0.5
    
# Main Execution

if __name__ == '__main__':
    unittest.main()
