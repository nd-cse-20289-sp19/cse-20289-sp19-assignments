Homework 06
===========

# Activity 1 - Questions

1. What is the difference between a shared library such as `libstr.so` and a
   static library such as `libstr.a`?

2. Compare the sizes of `libstr.so` and `libstr.a`, which one is larger? Why?

# Activity 2 - Questions

1. What is the difference between a static executable such as `str-static`
   and a dynamic executable such as `str-dynamic`?

2. Compare the sizes of `str-static` and `str-dynamic`, which one is larger?
   Why?

3. Login into a new shell and try to execute `str-dynamic`.  Why doesn't
   `str-dynamic` work?  Explain what you had to do in order for `str-dynamic`
   to actually run.

4. Login into a new shell and try to execute `str-static`.  Why does
   `str-static` work, but `str-dynamic` does not in a brand new shell session?
