#!/bin/bash

SCRIPT=sort.py
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

export LC_ALL=C

echo "Testing $SCRIPT ..."

printf " %-40s ... " "Usage"
./$SCRIPT -h 2>&1 | grep -i usage 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    error "Failed Usage Test"
else
    echo "Success"
fi

for path in /etc/group /etc/passwd; do
    printf " %-40s ... " "sort    on $path"
    ./$SCRIPT $path | diff -y - <(sort $path) > $WORKSPACE/test
    if [ $? -ne 0 ]; then
	error "Failure"
    else
    	echo "Success"
    fi
    printf " %-40s ... " "sort -r on $path"
    ./$SCRIPT -r $path | diff -y - <(sort -r $path) > $WORKSPACE/test
    if [ $? -ne 0 ]; then
	error "Failure"
    else
    	echo "Success"
    fi
done

printf " %-40s ... " "sort    on stdin (implicit)"
cat /etc/passwd | ./$SCRIPT | diff -y - <(cat /etc/passwd | sort) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "sort -r on stdin (implicit)"
cat /etc/passwd | ./$SCRIPT -r | diff -y - <(cat /etc/passwd | sort -r) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "sort    on stdin (explicit)"
cat /etc/passwd | ./$SCRIPT - | diff -y - <(cat /etc/passwd | sort -) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "sort -r on stdin (explicit)"
cat /etc/passwd | ./$SCRIPT -r - | diff -y - <(cat /etc/passwd | sort -r -) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

TESTS=9
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 1.5" | bc | awk '{printf "%0.2f\n", $1}')"
echo
