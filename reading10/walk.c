/* walk.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    /* Open directory handle */
    DIR *d = ____(".");                                                     /* 1 */
    if (!d) {
    	fprintf(stderr, "%s\n", ____(errno));                               /* 2 */
    	return EXIT_FAILURE;
    }

    /* For each directory entry, check if it is a file, and print out the its
     * name and file size */
    for (struct dirent *e = ____(d); e; e = ____(d)) {                      /* 3 & 4 */
        /* Skip current directory and parent directory */
        if (strcmp(____, ".") == 0 || strcmp(____, "..") == 0) {            /* 5 & 6 */
            continue;
        }

        /* Skip non-regular files */
	if (____ != DT_REG) {                                               /* 7 */
	    continue;
        }

        /* Get file meta-data */
	struct stat s;
	if (____(____, &s) < 0) {                                           /* 8 & 9 */
	    fprintf(stderr, "%s\n", ____(errno));                           /* 10 */
	    continue;
        }

        /* Display file name and size */
	printf("%s %lu\n", ____, ____);                                     /* 11 & 12 */
    }

    /* Close directory handle */
    ____(d);                                                                /* 13 */
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
