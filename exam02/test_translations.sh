#!/bin/bash

WORKSPACE=/tmp/translations.$(id -u)
FAILURES=0

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

echo "Testing translations ..."

printf " %-40s ... " "translate1.py"
./translate1.py | diff -wy - <(cat /etc/passwd | cut -d : -f 7 | sort | uniq -c | sort -rn) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "translate2.py"
./translate2.py | diff -y - <(curl -sLk https://yld.me/raw/Hk1 | sort -t , -k 2 | cut -d , -f 1 | tr a-z A-Z) > $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

TESTS=$(($(grep -c Success $0) - 1))
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 7.0" | bc | awk '{printf "%0.2f\n", $1}')"
